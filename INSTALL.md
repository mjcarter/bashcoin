Building Bashcoin
================

See doc/build-*.md for instructions on building the various
elements of the Bashcoin Core reference implementation of Bashcoin.
